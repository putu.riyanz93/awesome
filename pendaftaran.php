
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Carousel Template · Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.2/examples/carousel/">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
      }

      .mt-10 {
          margin-top: 6rem !important;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
  </head>
  <body>
    <header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">LAYANAN MASYARAKAT</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="pendaftaran.php">Pengaduan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="result.php">Daftar Aduan</a>
        </li>
      </ul>
    </div>
  </nav>
</header>

<main role="main">

    <div class="container">
      <h2 class="alert alert-danger text-center mt-10 mb-5">FORMULIR PENDAFTARAN</h2>
      <form action="result.php">
        <div class="form-group">
         <label for="Nama Lengkap">Nama Lengkap</label>
         <input type="text" name="" class="form-control" placeholder="Masukkan Nama">
        </div>
         <div class="form-group mt-2">
         <label for="Tempat Lahir">NIK</label>
         <input type="text" name="" class="form-control" placeholder="Masukkan NIK">
        </div>
         <div class="form-group mt-2">
         <label for="Tempat Lahir">NO HP</label>
         <input type="text" name="" class="form-control" placeholder="Masukkan NO HP">
        </div>
        <div class="form-group mt-2">
         <label for="Tempat Lahir">EMAIL</label>
         <input type="text" name="" class="form-control" placeholder="Masukkan EMAIL">
        </div>
        <div class="form-group mt-2">
         <label for="Tempat Lahir">Alamat</label>
         <input type="text" name="" class="form-control" placeholder="Masukkan Alamat">
        </div>
        <div class="form-group mt-2">
         <label for="Tanggal Lahir">Tanggal Lahir</label>
         <input type="date" name="" class="form-control" placeholder="Masukkan Tanggal Lahir">
        </div>
          <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModal">SIMPAN</a>
          <a href="result.php" class="btn btn-primary float-right">LIHAT DAFTAR ANTRIAN</a>
        </div>
      </form>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Notifikasi</h4>
          </div>
          <div class="modal-body">
            <p>Data berhasil disimpan!</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
</main>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>
