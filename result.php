
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Carousel Template · Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.2/examples/carousel/">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
      }

      .mt-10 {
          margin-top: 6rem !important;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
  </head>
  <body>
    <header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">LAYANAN MASYARAKAT</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="pendaftaran.php">Pengaduan</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="result.php">Daftar Aduan</a>
        </li>
      </ul>
    </div>
  </nav>
</header>

<main role="main">

<div class="container">
      <h2 class="alert alert-danger text-center mt-10 mb-5">DAFTAR ADUAN</h2>
      <a href="pendaftaran.php" class="btn btn-primary mb-5" >Tambah Data</a>
      <table class="table">
          <thead>
              <tr>
                  <th scope="col">No</th>
                  <th scope="col">Nama</th>
                  <th scope="col">NIK</th>
                  <th scope="col">No HP</th>
                  <th scope="col">Email</th>
                  <th scope="col">Alamat</th>
                  <th scope="col">Tanggal Lahir</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <td>1</td>
                  <td>Andera Mukti</td>
                  <td>110030365</td>
                  <td>081338760743</td>
                  <td>andera@gmail.com</td>
                  <td>Jalan Harapan Bangsa</td>
                  <td>22/12/1992</td>
              </tr>
              <tr>
                  <td>2</td>
                  <td>Hery Darma Yuda</td>
                  <td>8006555349</td>
                  <td>089886969679</td>
                  <td>hery@gmail.com</td>
                  <td>Jalan raya blahkiuh</td>
                  <td>31/03/1993</td>
              </tr>
              <tr>
                  <td>3</td>
                  <td>Bayu Ganteng</td>
                  <td>8963524130</td>
                  <td>08098999999</td>
                  <td>Bayu@gmail.com</td>
                  <td>Jalan Raya Abianbase</td>
                  <td>01/0306/1994</td>
              </tr>
          </tbody>
      </table>
    </div>
</main>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>
